CREATE VIEW ManageEmployee AS
SELECT e.EmployeeID,
e.EmployeeName,
e.EmployeeSurname,
d.DeptName,
m.ManagerName,
m.ManagerSurname
FROM Employee AS e
		INNER JOIN
	Department d 
		INNER JOIN
	Manager m 
ORDER BY EmployeeID desc;
